import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    agents_url : 'https://agents.davao.me/',
    agents_url_register : 'https://agents.davao.me/register',
    agents_url_login : 'https://agents.davao.me/login',
    agents_how_video : '#',
    hero_video : '#',
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
