import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import HomePage from '../views/HomePage.vue'
import products_routes from './products'
import services_routes from './services'
import quickbooks_routes from './quickbooks'

Vue.use(VueRouter)

const main_routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'homepage',
    component: HomePage
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutPage.vue'),
  },
  {
    path: '/products',
    name: 'products',
    component: () => import(/* webpackChunkName: "products" */ '../views/ProductsPage.vue')
  },
  {
    path: '/services',
    name: 'services',
    component: () => import(/* webpackChunkName: "services" */ '../views/ServicesPage.vue')
  },
  {
    path: '/team',
    name: 'team',
    component: () => import(/* webpackChunkName: "team" */ '../views/TeamPage.vue')
  },
  {
    path: '/contact',
    name: 'contact',
    component: () => import(/* webpackChunkName: "contact" */ '../views/ContactPage.vue')
  },
  {
    path: '/join-us',
    name: 'join-us',
    component: () => import(/* webpackChunkName: "join-us" */ '../views/JoinUsPage.vue')
  },
  {
    path: '/how-it-works',
    name: 'how-it-works',
    component: () => import(/* webpackChunkName: "how-it-works" */ '../views/HowItWorksPage.vue')
  }
];

const new_routes = main_routes.concat( products_routes ).concat( services_routes ).concat( quickbooks_routes );
let new_set = new Set( new_routes );
const routes = Array.from(new_set);

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

export default router
