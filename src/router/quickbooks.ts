const quickbooks_routes = [

  // quickbooks
  {
    path: '/quickbooks/check-voucher-print',
    name: 'quickbooks-1',
    component: () => import(/* webpackChunkName: "quickbooks-check-voucher-print" */ '../views/quickbooks/CheckVoucherPrintPage.vue')
  },
  {
    path: '/quickbooks/advances-for-liquidation',
    name: 'quickbooks-2',
    component: () => import(/* webpackChunkName: "quickbooks-advances-for-liquidation" */ '../views/quickbooks/AdvancesLiquidationPage.vue')
  },

];

export default quickbooks_routes
