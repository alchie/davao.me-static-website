const products_routes = [

  // products
  {
    path: '/products/cooperative-information-system',
    name: 'products-1',
    component: () => import(/* webpackChunkName: "products-cooperative-information-system" */ '../views/products/CooperativeSystemPage.vue')
  },
  {
    path: '/products/school-information-system',
    name: 'products-2',
    component: () => import(/* webpackChunkName: "products-school-information-system" */ '../views/products/SchoolSystemPage.vue')
  },
  {
    path: '/products/hr-and-payroll-system',
    name: 'products-3',
    component: () => import(/* webpackChunkName: "products-hr-and-payroll-system" */ '../views/products/PayrollSystemPage.vue')
  },
  {
    path: '/products/client-information-system',
    name: 'products-4',
    component: () => import(/* webpackChunkName: "products-client-information-system" */ '../views/products/ClientSystemPage.vue')
  },
  {
    path: '/products/hospital-information-system',
    name: 'products-5',
    component: () => import(/* webpackChunkName: "products-hospital-information-system" */ '../views/products/HospitalSystemPage.vue')
  },
  {
    path: '/products/finance-information-system',
    name: 'products-6',
    component: () => import(/* webpackChunkName: "products-finance-information-system" */ '../views/products/FinanceSystemPage.vue')
  },
  {
    path: '/products/online-testbank-system',
    name: 'products-7',
    component: () => import(/* webpackChunkName: "products-online-testbank-system" */ '../views/products/TestbankSystemPage.vue')
  },
  {
    path: '/products/lesson-plan-system',
    name: 'products-8',
    component: () => import(/* webpackChunkName: "products-lesson-plan-system" */ '../views/products/LessonPlanSystemPage.vue')
  },
  {
    path: '/products/student-attendance-monitoring-system',
    name: 'products-9',
    component: () => import(/* webpackChunkName: "products-student-attendance-monitoring-system" */ '../views/products/StudentAttendanceSystemPage.vue')
  },
  {
    path: '/products/quickbooks-support-systems',
    name: 'products-10',
    component: () => import(/* webpackChunkName: "products-quickbooks-support-systems" */ '../views/products/QuickbooksSupportSystemsPage.vue')
  },
];

export default products_routes
